//alert("hello");

// Variables

const inputBtn = document.getElementById('input-details-btn');
//let subForm = document.getElementById('sub-form');
let playerNumber;
const form = document.querySelector('form');
let player1Name;
let player2Name;
let moves = 0;
let selectedBlocks = [];
let xblocks = [];
let yblocks = [];
let grid = document.querySelector('#back-grid');
let check;
let lost;
let playerWon = "";
let playerWonName = "";
const winCondition = [
    ['box1', 'box2', 'box3'],
    ['box1', 'box4', 'box7'],
    ['box1', 'box5', 'box9'],
    ['box2', 'box5', 'box8'],
    ['box3', 'box6', 'box9'],
    ['box3', 'box5', 'box7'],
    ['box4', 'box5', 'box6'],
    ['box7', 'box8', 'box9']
]

const resetBtn = document.getElementById('reset-btn');

// HELPER FUNCTIONS

// Show Function
function show(id){
    document.getElementById(id).style.display = 'block';
}

// Hide Function
function hide(id){
    document.getElementById(id).style.display = 'none';
}

// Set Text Funxtion
function setText(id, text){
    document.getElementById(id).innerHTML = text;
}

// Initalize all events

inputBtn.addEventListener('click', playerDetails);
document.body.addEventListener('click', playerNo);
form.addEventListener('submit', submitDetails);
resetBtn.addEventListener('click', resetGame);



// Event Functions
function playerDetails(e){
    show('details-block');
    hide(e.target.id);
    hide('sub-form');
}

function playerNo(e){
    if(e.target.id === "no-of-players1"){
        show('sub-form');
        hide('player2');
        hide('player2-l');
        playerNumber = 1;
    }
    if(e.target.id === "no-of-players2"){
        show('sub-form');
        show('player2');
        show('player2-l');
        playerNumber = 2;
    }
}

function submitDetails(e){
    e.preventDefault();
    moves = 0;
    if(document.getElementById('player1').value === ""){
        alert("Enter a name for player 1");
        } else{
            if(playerNumber === 1){
            player1Name = document.getElementById('player1').value;
            player2Name = 'Computer';
            console.log(player1Name);  
            hide('details-block');
            alert(`It\'s ${player1Name} vs ${player2Name}`);
            show('moves');
            show('playername');
            show('reset-btn');
            setText('player1name', player1Name);
            setText('player2name', player2Name);
            setText('moves-value', moves);
            console.log(playerNumber);
            vsComputer();
        
        } else{
            if(document.getElementById('player2').value === ""){
                alert("Enter a name for player 2");
            } else{
                player1Name = document.getElementById('player1').value;
                player2Name = document.getElementById('player2').value;
                console.log(player1Name);
                console.log(player2Name);
                hide('details-block');
                alert(`It\'s ${player1Name} vs ${player2Name}`);
                show('moves');
                show('playername');
                show('reset-btn');
                setText('player1name', player1Name);
                setText('player2name', player2Name);
                setText('moves-value', moves);
                console.log(playerNumber);
                vsPlayer();
            }
        } 
    }
}

function clickOnGrid2(e){
    if(e.target.classList.contains('block')){
            if(selectedBlocks.includes(e.target.id) && (check!=1)){
                alert('Cannot Repeat Any Already Clicked Block!');
            }else{
                if(check === 2){
                    if(moves % 2 === 0){
                        setText(e.target.id, 'X');
                        moves++;
                        setText('moves-value', moves);
                        selectedBlocks.push(e.target.id);
                        xblocks.push(e.target.id);
                        console.log(selectedBlocks);
                        checkXWin();
                        if(moves == 9){
                            resetDrawGame();
                        }
                    } else{
                        setText(e.target.id, 'O');
                        moves++;
                        setText('moves-value', moves);
                        selectedBlocks.push(e.target.id);
                        yblocks.push(e.target.id);
                        console.log(selectedBlocks);
                        checkYWin();
                        if(moves == 9){
                            resetDrawGame();
                        }
                    }
                }
            }
        }   
}

function clickOnGrid1(e){
    if(e.target.classList.contains('block')){
            if(selectedBlocks.includes(e.target.id) && (check!=2)){
                alert('Cannot Repeat Any Already Clicked Block!');
            }else{
                if(check === 1){
                    if(moves % 2 === 0){
                        setText(e.target.id, 'X');
                        moves++;
                        setText('moves-value', moves);
                        selectedBlocks.push(e.target.id);
                        xblocks.push(e.target.id);
                        console.log(selectedBlocks);
                        checkXWin();
                        if(playerWon === ""){
                            let timeGap = setTimeout(function(){
                            let randomPosition
                            do{
                                randomPosition = Math.round((1 + Math.random()*8));;
                            } while(selectedBlocks.includes('box'+randomPosition));
                            
                            setText(`box${randomPosition}`, 'O');
                            moves++;
                            setText('moves-value', moves);
                            selectedBlocks.push(`box${randomPosition}`);
                            yblocks.push(`box${randomPosition}`);
                            checkYWin();
                            }, 500);
                        }
                    }
                }
            }
        }   
}

function vsPlayer(){
    grid.addEventListener('click', clickOnGrid2);
    check = 2;
    
}

function vsComputer(){
    grid.addEventListener('click', clickOnGrid1);
    check = 1;
}

function xWinForBlock(){
    
    winCondition.forEach(function(element, index){
        if(xblocks.includes(element[0]) && xblocks.includes(element[1]) && xblocks.includes(element[2]) ){
            console.log(`${player1Name} WON!!`);
            playerWon = "Player 1";
            playerWonName = player1Name;
            resetWinGame(yblocks);
            
        }
    });
//   if(xblocks.includes("box1")){
//        if(xblocks.includes("box2") && xblocks.includes("box3")){
//            console.log('X WON');
//        } else{
//            if(xblocks.includes("box4") && xblocks.includes("box7")){
//                console.log('X WON');
//            } else{
//                if(xblocks.includes("box5") && xblocks.includes("box9")){
//                console.log('X WON');
//            }  
//            }
//        }
//    } 
}

function yWinForBlock(){
    
    winCondition.forEach(function(element, index){
        if(yblocks.includes(element[0]) && yblocks.includes(element[1]) && yblocks.includes(element[2]) ){
            console.log(`${player2Name} WON!!`);
            playerWon = "Player 2";
            playerWonName = player2Name;
            resetWinGame(xblocks);
        }
    }); 
}

function checkXWin(){
    xWinForBlock();
}

function checkYWin(){
    yWinForBlock();
}

function winColor(e){
    if(e.target.id === 'win-block'){
        e.target.style.background = `rgb(${e.offsetX}, ${e.offsetY}, ${Math.random()*255})`;
    }
}

function resetGame(){
    console.log(xblocks);
    console.log(yblocks);
    player1Name = player2Name = playerWon = playerWonName = "";
    moves = 0;
    playerNumber = 0;
    check = 0;
    setText('player1name', player1Name);
    setText('player2name', player2Name);
    setText('moves-value', moves);
    setText('player-num', playerWon);
    setText('player-name', playerWonName);
    setText('won-moves-value', moves);
    selectedBlocks.forEach(function(boxid, index){
        document.getElementById(boxid).innerHTML = "";
    })
    selectedBlocks = [];
    xblocks = [];
    yblocks = [];
    hide('moves');
    hide('playername');
    hide('reset-btn');
    hide('win-block');
    show('input-details-btn');
    
}
function resetWinGame(lost){
    console.log(xblocks);
    console.log(yblocks);
//    player1Name = player2Name = "";
//    moves = 0;
//    playerNumber = 0;
//    check = 0;
//    setText('player1name', player1Name);
//    setText('player2name', player2Name);
//    setText('moves-value', moves);
    lost.forEach(function(boxid, index){
        document.getElementById(boxid).innerHTML = "";
    })
//    selectedBlocks = [];
//    xblocks = [];
//    yblocks = [];
    setText('player-num', playerWon);
    setText('player-name', playerWonName);
    setText('won-moves-value', moves);
    show('win-block');
    document.getElementById('win-block').addEventListener('mouseover', winColor);
    hide('moves');
    hide('playername');
//    hide('reset-btn');
//    show('input-details-btn');
    
}
function resetDrawGame(){
    setText('player-num', 'None');
    setText('player-name', "It\'s A Draw! No");
    setText('won-moves-value', moves);
    show('win-block');
    document.getElementById('win-block').addEventListener('mouseover', winColor);
    hide('moves');
    hide('playername');   
}